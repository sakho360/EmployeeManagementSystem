<?php
require_once "vendor/autoload.php";
$message = '';

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Human Resource Management Sysytem</title>

    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="assets/css/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="assets/css/animate.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="assets/css/custom.css" rel="stylesheet">
</head>

<body class="login">
<div>
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>

    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">

                    <?php
                    $message = \Sandwitch\Utility\Utility::flushMessage();
                    if ($message){
                        echo "<div class=\"alert alert-danger\" role=\"alert\">";
                        echo "<strong>OOP's ! </strong>".$message;
                        echo " </div>";
                    }

                    ?>

                <form action="views/login/login.php" method="post">

                    <h1><a href="index.php">Log in Admin</a></h1>
                    <h1><a href="users/index.php">Employee Log in</a></h1>

                    <div>
                        <input type="text" id="userName" name="userName" class="form-control" placeholder="Username" required="" />
                    </div>
                    <div>
                        <input type="password" id="password" name="password" class="form-control" placeholder="Password" required="" />
                    </div>
                    <div>
                        <button type="submit" name="login" class="btn btn-default submit">Log in</button>
                    </div>

                    <div class="clearfix"></div>

                    <div class="separator">
<!--                        <p class="change_link">New to site?-->
<!--                            <a href="#signup" class="to_register"> Create Account </a>-->
<!--                        </p>-->
<!---->
<!--                        <div class="clearfix"></div>-->
<!--                        <br />-->

                        <div>
                            <h1><i class="fa fa-paw"></i> Sandwitch Group !</h1>
                            <p>©2017 All Rights Reserved. Sandwitch Group</p>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
    </div>

</div>

</body>
</html>

