<?php

require_once "../../vendor/autoload.php";

use Sandwitch\Designation\Designation;
use Sandwitch\Logger\DesignationMessage;

$designationMessage = new DesignationMessage();
$designation = new Designation($designationMessage);
$designation->add($_POST);

