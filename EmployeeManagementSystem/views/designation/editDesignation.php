<?php
require_once "../../vendor/autoload.php";

use Sandwitch\Designation\Designation;
use Sandwitch\Logger\DesignationMessage;

$designationMessage = new DesignationMessage();
$obj = new Designation($designationMessage);
$designation = $obj->show($_GET['id']);
foreach ($designation as $row){
    $designation = $row;
}
?>

<!--header-->
    <?php require_once "../elements/header.php"?>
<!--header-->
<body class="nav-md">
<div class="container body">
    <div class="main_container">

        <!-- side and top bar include -->
        <?php include '../elements/nav.php' ?>
        <!-- /side and top bar include -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                              <button class="btn btn-default" type="button">Go!</button>
                          </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">

                                <form action="editDesignationStore.php" method="post" class="form-horizontal form-label-left" novalidate>

                                    <span class="section">Updated designation of a employee</span>

                                    <div class="item form-group">
                                        <input id="id" class="form-control col-md-7 col-xs-12" data-validate-minmax="" name="id" required="required" type="hidden" value="<?php echo $designation->id; ?>">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="eventSubject">Name <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="name" class="form-control col-md-7 col-xs-12" data-validate-minmax="" name="name" placeholder="Senior Programmer" required="required" type="text" value="<?php echo $designation->title; ?>">
                                        </div>
                                    </div>

                                    <div class="ln_solid"></div>

                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-3">
                                            <button name="btn-notice" id="btn-notice" type="submit" class="btn btn-success">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content include -->
        <?php include '../elements/footer.php' ?>
        <!-- /footer content include -->
    </div>
</div>

<!--script-->
    <?php require_once "../elements/script.php"?>
<!--script-->
</body>
</html>
