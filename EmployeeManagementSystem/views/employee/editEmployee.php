<?php

require_once "../../vendor/autoload.php";

use Sandwitch\Department\Department;
use Sandwitch\Designation\Designation;
use Sandwitch\Logger\DesignationMessage;
use Sandwitch\Logger\DepartmentMessage;
use Sandwitch\Logger\UserMessage;
use Sandwitch\Employee\Employee;

$userMessage = new UserMessage();
$employee = new Employee($userMessage);
$employee = $employee->show($_GET['id']);

$departmentMessage = new DepartmentMessage();
$department = new Department($departmentMessage);

$designationMessage = new DesignationMessage();
$designation = new Designation($designationMessage);

foreach ($employee as $employees) {
        $employee = $employees;
}
$departments = $department->getAll();
$designations = $designation->getAll();
?>
<!--header-->
<?php require_once "../elements/header.php"?>
<!--header-->

<body class="nav-md">
<div class="container body">
    <div class="main_container">

        <!-- side and top bar include -->
        <?php include '../elements/nav.php' ?>
        <!-- /side and top bar include -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">>
                        <h3>Edit Employee</h3>
                    </div>

                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                              <button class="btn btn-default" type="button">Go!</button>
                          </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Put your employee information <small>correctly</small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">

                                <form name="edit_employee" class="form-horizontal form-label-left" novalidate action="editEmployeeStore.php" method="post">

                                    <span class="section">Employee Info</span>

                                    <div class="item form-group">
                                        <input id="id" class="form-control col-md-7 col-xs-12" data-validate-length-range="" data-validate-words="" name="id" placeholder="Jon" required="required" type="hidden" value="<?= $employee->id; ?>">

                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="firstName">First Name <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="firstName" class="form-control col-md-7 col-xs-12" data-validate-length-range="" data-validate-words="" name="firstName" placeholder="Jon" required="required" type="text" value="<?= $employee->first_name; ?>">
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lastName">Last Name <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="lastName" class="form-control col-md-7 col-xs-12" data-validate-length-range="" data-validate-words="" name="lastName" placeholder="Doe" required="required" type="text" value="<?= $employee->last_name; ?>">
                                        </div>
                                    </div>


                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="userName">User Name <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="userName" class="form-control col-md-7 col-xs-12" data-validate-length-range="" data-validate-words="" name="userName" placeholder="Doe" required="required" type="text" value="<?= $employee->user_name; ?>">
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label for="password" class="control-label col-md-3">Password<span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="password" type="password" name="password" data-validate-length="" class="form-control col-md-7 col-xs-12" required="required" value="<?= $employee->password; ?>">
                                        </div>
                                    </div>


                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="department">Department <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select id="department_id" name="department_id" >
                                                <?php foreach ($departments as $department) {?>
                                                    <option value="<?=$department->id;?>"><?=$department->name;?></option>

                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="designation">Designation <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select id="designation_id" name="designation_id">
                                                <?php foreach ($designations as $designation) {?>
                                                    <option value="<?=$designation->id; ?>"><?=$designation->title; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label for="email" class="control-label col-md-3">Email</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="email" type="email" name="email" data-validate-length="" class="form-control col-md-7 col-xs-12" required="required" placeholder="ex@mail.com" value="<?= $employee->email; ?>">
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Number <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" id="number" name="number" required="required" data-validate-minmax="" class="form-control col-md-7 col-xs-12" value="<?= $employee->mobile_no; ?>">
                                        </div>
                                    </div>

                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-3">
                                            <button type="submit" class="btn btn-primary">Cancel</button>
                                            <button id="send" type="submit" class="btn btn-success">Submit</button>
                                        </div>
                                    </div>
                                </form>

                                <script type="text/javascript">
                                    document.forms['edit_employee'].elements['department_id'].value="<?= $employee->department_id;?>";
                                    document.forms['edit_employee'].elements['designation_id'].value="<?= $employee->designation_id; ?>";
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content include -->
        <?php include '../elements/footer.php' ?>
        <!-- /footer content include -->
    </div>
</div>

<!--script-->
<?php require_once "../elements/script.php"?>
<!--script-->

</body>
</html>
