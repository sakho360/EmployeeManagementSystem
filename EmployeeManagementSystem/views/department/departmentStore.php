<?php

require_once "../../vendor/autoload.php";

use Sandwitch\Department\Department;
use Sandwitch\Logger\DepartmentMessage;

$departmentMessage = new DepartmentMessage();
$department = new Department($departmentMessage);
$department->add($_POST);
