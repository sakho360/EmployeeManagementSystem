<?php
require_once "../../vendor/autoload.php";

use Sandwitch\Department\Department;
use Sandwitch\Logger\DepartmentMessage;

$departmentMessage = new DepartmentMessage();
$department = new Department($departmentMessage);

$departments = $department->getAll();

?>
<?php
$message = \Sandwitch\Utility\Utility::flushMessage();
?>

<!--header-->
    <?php require_once "../elements/header.php"?>
<!--header-->

<body class="nav-md">
<div class="container body">
    <div class="main_container">

        <!-- side and top bar include -->
        <?php include '../elements/nav.php' ?>
        <!-- /side and top bar include -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="page-title">
                    <div class="title_left">

                        <h3>Department History</h3>
                        <?php
                        if (!is_null($message)){
                            echo "<div style='width: 222%' class=\"alert alert-success\" role=\"alert\">";
                            echo $message;
                            echo " </div>";
                        }

                        ?>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Manage <small>Department</small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">

                                <p class="text-muted font-13 m-b-30">

                                </p>

                                <table id="datatable-buttons" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Deartment Name</th>
                                        <th>Created_by</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($departments as $department){
                                        ?>
                                        <tr>
                                            <td><?= $department->name; ?></td>
                                            <td><?= $department->create_by; ?></td>
                                            <td>
                                                <?php
                                                if($department->status == 1){
                                                    echo "Active";
                                                }else{
                                                    echo "InActive";
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <a class="btn btn-primary" title="show" href="showDepartment.php?id=<?php echo $department->id; ?>">
                                                    <span class="glyphicon glyphicon-education"></span>
                                                </a>
                                                <a class="btn btn-success" title="edit" href="editDepartment.php?id=<?php echo $department->id; ?>">
                                                    <span class="glyphicon glyphicon-edit"></span>
                                                </a>
                                                <a class="btn btn-danger" title="delete" href="deleteDepartment.php?id=<?php echo $department->id; ?>">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content include -->
        <?php include '../elements/footer.php' ?>
        <!-- /footer content include -->
    </div>
</div>
<!--script-->
    <?php require_once "../elements/script.php"?>
<!--script-->
</body>
</html>
