<?php
require_once "../../vendor/autoload.php";

use Sandwitch\Department\Department;
use Sandwitch\Logger\DepartmentMessage;

$departmentMessage = new DepartmentMessage();
$obj = new Department($departmentMessage);
$department = $obj->show($_GET['id']);
foreach ($department as $row){
    $department = $row;
}
?>

<!--header-->
    <?php require_once "../elements/header.php"?>
<!--header-->

<body class="nav-md">
<div class="container body">
    <div class="main_container">

        <!-- side and top bar include -->
        <?php include '../elements/nav.php' ?>
        <!-- /side and top bar include -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="page-title">
                    <div class="title_left">
                        <h3>Department History</h3>
                    </div>

                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">

                                <p class="text-muted font-13 m-b-30">

                                </p>
                                <div class="list-group">
                                    <a href="#" class="list-group-item active">
                                        Department Details
                                    </a>
                                    <a href="#" class="list-group-item"> <strong> Name :</strong> <?php echo $department->name;?></a>
                                    <a href="#" class="list-group-item"><strong> Create By :</strong> <?php echo $department->create_by;?></a>
                                    <a href="#" class="list-group-item"><strong> Status :</strong> <?php
                                        if ($department->status == 1){
                                            echo "Active";
                                        }else{
                                            echo "Inactive";
                                        }
                                        ?>
                                    </a>
                                    <a href="#" class="list-group-item"> <strong> Created At :</strong> <?php echo $department->create_date;?></a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content include -->
        <?php include '../elements/footer.php' ?>
        <!-- /footer content include -->
    </div>
</div>

<!--script-->
    <?php require_once "../elements/script.php"?>
<!--script-->
</body>
</html>
