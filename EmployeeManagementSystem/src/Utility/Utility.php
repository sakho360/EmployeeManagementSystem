<?php

namespace Sandwitch\Utility;


class Utility
{
    static function redirect($to = '')
    {
        header("location:$to");
    }

    static public function setMessage($message = ""){
        session_start();
        $_SESSION['message'] = $message;
    }

    static public function flushMessage(){
        session_start();

        if(!empty($_SESSION['message'])){
            $message = $_SESSION['message'];
            $_SESSION['message'] = "";
            return $message ;
        }
        return "";
    }
    static public function setSession($name)
    {
        session_start();
        $_SESSION['userName'] = $name;
    }

    static function checkSession()
    {
        session_start();
        if (empty($_SESSION['userName'])){
            //echo $_SESSION['userName'];
            header('location:../../index.php');
        }
    }
}