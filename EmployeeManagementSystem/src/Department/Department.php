<?php

namespace Sandwitch\Department;

use Sandwitch\Common\Common;
use Sandwitch\Db\Connection;
use Sandwitch\Logger\notifier;
use Sandwitch\Utility\Utility;
use Sandwitch\Utility\Senitaize;
use PDO;

class Department implements Common
{
    public $notifier;
    public function __construct(notifier $obj)
    {
        $this->notifier = $obj;
    }

    public function add($data)
    {
        if (is_null($data)){
            return;
        }
        $name = $data['name'];
        $created = 'admin';
        $created_date = 'NOW';
        $con = new Connection();
        $db = $con->openConnection();
        $stm = $db->prepare("INSERT INTO `departments` (`name`,`create_by`, `create_date`) 
              VALUES (:department, :crated, :date_by)");

        $stm->bindParam(':department', $name, PDO::PARAM_STR);
        $stm->bindParam(':crated', $created, PDO::PARAM_STR);
        $stm->bindParam(':date_by', $created_date, PDO::PARAM_STR);
        $result = $stm->execute();

        if($result) {
            $msg = $this->notifier->setMessage(" Has been successfully Created");
            Utility::setMessage($msg);
            Utility::redirect("../../views/department/department.php");

        } else {
            $msg = $this->notifier->setMessage("There is some error.Please try again.");
            Utility::setMessage($msg);
            Utility::redirect("../../views/department/department.php");
        }
    }

    public function getAll()
    {
        $con = new Connection();
        $db = $con->openConnection();
        $stm = $db->query("SELECT * FROM `departments` ORDER BY id DESC ");
        $departments = $stm->fetchAll(PDO::FETCH_OBJ);
        return $departments;
    }
    
    public function show($id)
    {
        $con = new Connection();
        $db = $con->openConnection();
        $stm = $db->prepare("SELECT * FROM `departments` WHERE id = :id");
        $stm->execute(array(':id' => $id));
        $department = $stm->fetchAll(PDO::FETCH_OBJ);
        return $department;
    }

    public function edit($data)
    {
        if (is_null($data)){
            return;
        }
        $con = new Connection();
        $db = $con->openConnection();
        $departmentName = $data['name'];
        $id = $data['id'];
        $modified_by = 'admin';
        $time = 'NOW()';
        $stm =$db->prepare("UPDATE `departments` SET `name` = ?, `modified_by` = ?,`modified_date` = ? WHERE                    `departments`.`id` = ? ");
        $stm->bindValue(1, $departmentName, PDO::PARAM_STR);
        $stm->bindValue(2, $modified_by, PDO::PARAM_STR);
        $stm->bindValue(3, $time, PDO::PARAM_STR);
        $stm->bindValue(4, $id, PDO::PARAM_INT);
        $stm->execute();
        $affected_rows = $stm->rowCount();

        if($affected_rows) {
            $msg = $this->notifier->setMessage(" Has been successfully Updated");
            Utility::setMessage($msg);
            Utility::redirect("../../views/department/departmentHistory.php");

        } else {
            $msg = $this->notifier->setMessage("There is some error.Please try again.");
            Utility::setMessage($msg);
            Utility::redirect("../../views/department/departmentHistory.php");
        }
    }

    public function delete($id)
    {
        if (is_null($id)){
            return;
        }
        $con = new Connection();
        $db = $con->openConnection();
        $stm=$db->prepare("DELETE FROM `departments` WHERE id = :id");
        $stm->bindParam(':id', $id, PDO::PARAM_INT);
        $stm->execute();
        $affected_rows = $stm->rowCount();

        if($affected_rows) {
            $msg = $this->notifier->setMessage(" Has been successfully Deleted");
            Utility::setMessage($msg);
            Utility::redirect("../../views/department/departmentHistory.php");

        } else {
            $msg = $this->notifier->setMessage("There is some error.Please try again.");
            Utility::setMessage($msg);
            Utility::redirect("../../views/department/departmentHistory.php");
        }

    }
    
    public function selectDepartmentByEmployee($id)
    {
        if (is_null($id)){
            return;
        }
        $con = new Connection();
        $db = $con->openConnection();
        $stm = $db->prepare("SELECT departments.name FROM `departments`
                                        INNER JOIN employees
                                        ON employees.department_id = departments.id
                                        WHERE employees.id= :id");
        $stm->execute(array(':id' => $id));
        $department = $stm->fetch(PDO::FETCH_OBJ);
        //Senitaize::d($department);
        return $department;
    }

    public function total()
    {
        $con = new Connection();
        $db = $con->openConnection();
        $stm = $db->prepare("SELECT * FROM `departments`");
        $stm->execute();
        $departsments = $stm->rowCount();
        return $departsments;
    }



}