<?php

namespace Sandwitch\Attendance;

use Sandwitch\Common\Common;
use Sandwitch\Db\Connection;
use Sandwitch\Logger\notifier;
use Sandwitch\Utility\Utility;
use Sandwitch\Utility\Senitaize;
use PDO;

class Attendance
{
    public function getAll()
    {
        $con = new Connection();
        $db = $con->openConnection();
        $stm = $db->query("SELECT * FROM `employee_attendance` ORDER BY id DESC ");
        $attendance = $stm->fetchAll(PDO::FETCH_CLASS,'\Sandwitch\Attendance\Attendance');
        return $attendance;
    }
}