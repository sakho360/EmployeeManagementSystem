<?php

namespace Sandwitch\UserLogIn;

use Sandwitch\Db\Connection;
use Sandwitch\Utility\Utility;
use Sandwitch\Utility\Senitaize;
use PDO;

class UserLogIn
{
    public function login($data)
    {
        $con = new Connection();
        $db = $con->openConnection();
        $name = $data['userName'];
        $password = $data['password'];
        $stmt = $db->prepare("SELECT * FROM employees WHERE user_name = ? AND password = ?");
        $stmt->bindParam(1, $name, PDO::PARAM_STR);
        $stmt->bindParam(2, $password, PDO::PARAM_STR);
        $stmt->execute();
        $rows = $stmt->fetch(PDO::FETCH_OBJ);
        $affected_row = $stmt->rowCount();
        if ($affected_row == true) {
            $date = date('Y-m-d');
            date_default_timezone_set('Asia/Dhaka');
            $time = date('h:i:s');
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }

            $unixTimestamp = strtotime($date);
            $dayOfWeek = date("l", $unixTimestamp);
            if ($ip != '127.0.0.1') {         //192.168.50.103
                echo "<p style='color: rgb(255, 0, 0);'>This is not Office</p>";
            } else {
                if ($dayOfWeek == 'Friday') {
                    echo "<p style='color: rgb(255, 0, 0);'>Today Is Off Day</p>";
                } else {
                    $stm = $db->prepare("INSERT INTO employee_attendance (username,log_in_time, log_in_date, ip)
                          VALUE ('$name', '$time', '$date', '$ip')");
                    $stm->execute();

                }

                if ($rows) {
                    $names = $rows->user_name;
                    //Senitaize::dd($names);
                    Utility::setSession($names);
                    $_SESSION['id'] = $rows->id;
                    // Senitaize::dd( $_SESSION['id']);
                    Utility::redirect("../../users/views/dashboard.php");
                } else {
                    Utility::setMessage("Username or Password does not match.");
                    Utility::redirect("../../users/index.php");
                }

            }
        }
    }
}