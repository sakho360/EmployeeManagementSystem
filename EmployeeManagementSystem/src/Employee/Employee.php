<?php
namespace Sandwitch\Employee;

use Sandwitch\Common\Common;
use Sandwitch\Db\Connection;
use Sandwitch\Logger\notifier;
use Sandwitch\Utility\Utility;
use PDO;


class Employee implements Common
{
    public $notifier;
    public function __construct(notifier $obj)
    {
        $this->notifier = $obj;
    }
    
    public function add($data)
    {
        if (is_null($data)){
            return;
         }
        $con = new Connection();
        $db = $con->openConnection();
        $firstName = $data['firstName'];
        $lastName = $data['lastName'];
        $userName = $data['userName'];
        $password = $data['password'];
        $email = $data['email'];
        $number = $data['number'];
        $department_id = $_POST['department_id'];
        $designation_id = $_POST['designation_id'];
        $created = 'admin';
        $time = 'NOW()';
        $query = "INSERT INTO `employees` (`first_name`, `last_name`, `user_name`, `password`, `email`, `department_id`,                        `designation_id`, `mobile_no`,`create_by`, `created_date`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $stm = $db->prepare($query);
        $result = $stm->execute(array($firstName, $lastName,$userName,$password,$email,$department_id,$designation_id,$number,                  $created, $time));
        if($result) {
            $msg = $this->notifier->setMessage(" Has been successfully Created");
            Utility::setMessage($msg);
            Utility::redirect("../../views/employee/createEmployee.php");

        } else {
            //Utility::setMessage($this->notifier->setMessage("There is some error.Please try again."));
            Utility::setMessage("There is some error.Please try again.");
            Utility::redirect("../../views/createEmployee.php");
        }

    }
    
    public function getAll()
    {
        $con = new Connection();
        $db = $con->openConnection();
        $stm = $db->query("SELECT * FROM `employees`");
        $employees = $stm->fetchAll(PDO::FETCH_OBJ);
        return $employees;
    }
    
    public function show($id)
    {
        $con = new Connection();
        $db = $con->openConnection();
        $stm = $db->prepare("SELECT * FROM `employees` WHERE id = :id");
        $stm->execute(array(':id' => $id));
        $employee = $stm->fetchAll(PDO::FETCH_OBJ);
        return $employee;
        
    }
    
    public function edit($data)
    {
        if (is_null($data)) {
            return;
        }
        $con = new Connection();
        $db = $con->openConnection();
        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
            $firstName = $data['firstName'];
            $lastName = $data['lastName'];
            $userName = $data['userName'];
            $password = $data['password'];
            $email = $data['email'];
            $department_id = $data['department_id'];
            $designation_id = $data['designation_id'];
            $mobile = $data['number'];
            $modified_by = 'admin';
            $time = 'NOW()';
            $id = $data['id'];

            $stm = $db->prepare("UPDATE `employees` SET `first_name` = ?, `last_name` = ?, `user_name` = ?, `password` = ?, `email` = ?, `department_id` = ?, `designation_id` = ?, `mobile_no` = ?,`modified_by` = ?, `modified_date` = ? WHERE `employees`.`id` = ?");
            $stm->bindValue(1, $firstName, PDO::PARAM_STR);
            $stm->bindValue(2, $lastName, PDO::PARAM_STR);
            $stm->bindValue(3, $userName, PDO::PARAM_STR);
            $stm->bindValue(4, $password, PDO::PARAM_STR);
            $stm->bindValue(5, $email, PDO::PARAM_STR);
            $stm->bindValue(6, $department_id, PDO::PARAM_STR);
            $stm->bindValue(7, $designation_id, PDO::PARAM_STR);
            $stm->bindValue(8, $mobile, PDO::PARAM_INT);
            $stm->bindValue(9, $modified_by, PDO::PARAM_STR);
            $stm->bindValue(10, $time, PDO::PARAM_STR);
            $stm->bindValue(11, $id, PDO::PARAM_INT);
            $stm->execute();
            $affected_rows = $stm->rowCount();

            if($affected_rows) {
                $msg = $this->notifier->setMessage(" Has been successfully Updated");
                Utility::setMessage($msg);
                Utility::redirect("../../views/employee/employeeHistory.php");

            } else {
                $msg = $this->notifier->setMessage("There is some error.Please try again.");
                Utility::setMessage($msg);
                Utility::redirect("../../views/employee/employeeHistory.php");
            }
        }
    }
    
    public function delete($id)
    {
        if (is_null($id)) {
            return;
        }
        $con = new Connection();
        $db = $con->openConnection();
        $stm=$db->prepare("DELETE FROM `employees` WHERE id = :id");
        $stm->bindParam(':id', $id, PDO::PARAM_INT);
        $stm->execute();
        $affected_rows = $stm->rowCount();

        if($affected_rows) {
            $msg = $this->notifier->setMessage(" Has been successfully Deleted");
            Utility::setMessage($msg);
            Utility::redirect("../../views/employee/employeeHistory.php");

        } else {
            $msg = $this->notifier->setMessage("There is some error.Please try again.");
            Utility::setMessage($msg);
            Utility::redirect("../../views/employee/employeeHistory.php");
        }

    }

    public function total()
    {
        $con = new Connection();
        $db = $con->openConnection();
        $stm = $db->prepare("SELECT * FROM `employees`");
        $stm->execute();
        $employees = $stm->rowCount();
        return $employees;
    }

}