<?php
namespace Sandwitch\Login;
require_once "../../lib/setting.php";

use Sandwitch\Db\Connection;
use Sandwitch\Utility\Utility;
use PDO;


class Login
{
    public function login($data)
    {
        $con = new Connection();
        $db = $con->openConnection();
        $name = $data['userName'];
        $password = $data['password'];
        $stmt = $db->prepare("SELECT * FROM admins WHERE name = ? AND password = ?");
        $stmt->bindParam(1, $name, PDO::PARAM_STR);
        $stmt->bindParam(2, $password, PDO::PARAM_STR);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($rows){
            $names = $rows[0]['name'];
            Utility::setSession($names);
            Utility::redirect("../../views/admin/dashboard.php");
        }else{
            Utility::setMessage("Username or Password does not match.");
            Utility::redirect("../../index.php");
        }

    }

}






/*

$admin ="";
foreach ($restlt as $row){
    $admin = $row;
}

$_SESSION['admin_name'] = $admin['name'];
$_SESSION['admin_password'] = $admin['password'];

if (isset($_POST['login'])){

    $user_name = trim($_POST['userName']);
    $user_password = trim($_POST['password']);

    if ($user_name == $_SESSION['admin_name'] && $user_password == $_SESSION['admin_password']) {
        header("Location:../../views/admin/dashboard.php");
    } else {
        header("Location:../../index.php?message=failed");
    }
}
*/
