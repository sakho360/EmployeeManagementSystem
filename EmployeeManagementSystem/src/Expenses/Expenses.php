<?php

namespace Sandwitch\Expenses;

use Sandwitch\Db\Connection;
use Sandwitch\Utility\Utility;
use PDO;

class Expenses
{
    public $employee;
    public $expenses;
    public $description;
    public $created_at;
    public function add($data)
    {
        if (is_null($data)){
            return;
        }
        $con = new Connection();
        $db = $con->openConnection();
        $employee = $data['employee'];
        $expenses = $data['expenses'];
        $description = $data['description'];
        $time = 'NOW()';
        $stm = $db->prepare("INSERT INTO `expenses` (`employee`, `expenses`, `description`, `created_at`) VALUES (? ,? , ?, ?)");
        $result = $stm->execute(array($employee, $expenses, $description, $time));
        if($result) {
            $msg = $this->notifier->setMessage(" Expenses Has been successfully Sended");
            Utility::setMessage($msg);
            Utility::redirect("../../users/views/expenses.php");

        } else {
            //Utility::setMessage($this->notifier->setMessage("There is some error.Please try again."));
            Utility::setMessage("There is some error.Please try again.");
            Utility::redirect("../../users/views/expenses.php");
        }
    }

    public function getAll()
    {
        $con = new Connection();
        $db = $con->openConnection();
        $stm = $db->query("SELECT * FROM `expenses` ORDER BY id DESC ");
        $expenses = $stm->fetchAll(PDO::FETCH_CLASS,'\Sandwitch\Expenses\Expenses');
        //Senitaize::dd($expenses);
        return $expenses;
    }



}