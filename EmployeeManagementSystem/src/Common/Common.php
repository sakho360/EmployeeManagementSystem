<?php

namespace Sandwitch\Common;

interface Common
{
    public function add($data);
    public function getAll();
    public function show($id);
    public function edit($data);
    public function delete($id);
    
}