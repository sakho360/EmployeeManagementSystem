<?php

namespace Sandwitch\Event;

use Sandwitch\Common\Common;
use Sandwitch\Db\Connection;
use Sandwitch\Logger\notifier;
use Sandwitch\Utility\Utility;
use PDO;

class Event
{
    public $notifier;
    public function __construct(notifier $obj)
    {
        $this->notifier = $obj;
    }

    public function add($data)
    {
        if (is_null($data)){
            return;
        }
        $con = new Connection();
        $db = $con->openConnection();
        $eventSubject=$data['eventSubject'];
        $eventDate=$data['eventDate'];
        $eventTime=$data['eventTime'];
        $eventDesc=$data['eventDesc'];
        $inserted = 'admin';
        $time = 'NOW';
        $stm = $db->prepare("INSERT INTO live_events (subject, event_date, event_time, description, insert_by, insert_date) 
     VALUES (? , ?, ?, ?, ?, ?)");
        $result = $stm->execute(array($eventSubject, $eventDate, $eventTime, $eventDesc, $inserted, $time));
        if($result) {
            $msg = $this->notifier->setMessage(" Has been successfully Created");
            Utility::setMessage($msg);
            Utility::redirect("../../views/liveEvent/sendLiveEvent.php");

        } else {
            //Utility::setMessage($this->notifier->setMessage("There is some error.Please try again."));
            Utility::setMessage("There is some error.Please try again.");
            Utility::redirect("../../liveEvent/sendLiveEvent.php");
        }
    }

    public function getAll()
    {
        $con = new Connection();
        $db = $con->openConnection();
        $stm = $db->query("SELECT * FROM `live_events` ORDER BY id DESC ");
        $events = $stm->fetchAll(PDO::FETCH_OBJ);
        return $events;

    }

}