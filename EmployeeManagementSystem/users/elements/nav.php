
<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="../views/dashboard.php" class="site_title"><i class="fa fa-paw"></i> <span>Sandwitch Group !</span></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_pic">
                <img src="../../assets/images/300.jpg" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                <h2>Sandwitch Group</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                    <li>
                        <a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                        <!--<ul class="nav child_menu">
                            <li><a href="../views/dashboard.php">Dashboard</a></li>
                        </ul>-->
                    </li>
                    <li>
                        <a><i class="fa fa-edit"></i> Employee <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="employeeHistory.php">Employee History</a></li>
                            <li><a href="liveEventHistory.php">Live Event History</a></li>
                            <!--<li><a href="../noticeBoard/sendNotice.php">Notice</a></li>
                            <li><a href="../noticeBoard/noticeHistory.php">Notice History</a></li>-->
                        </ul>
                    </li>
                  <!--  <li>
                        <a><i class="fa fa-table"></i> Attendance <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="tables_dynamic.html">View Attendance</a></li>
                        </ul>
                    </li>-->
                    <li>
                        <a><i class="fa fa-table"></i> Expanse <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <!--<li><a href="tables.html">Expanse Apply</a></li>-->
                            <li><a href="../views/expenses.php">Expanse</a></li>
                        </ul>
                    </li>
                 <!--   <li>
                        <a><i class="fa fa-bar-chart-o"></i> Leaves <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="chartjs.html">Leaves</a></li>
                            <li><a href="chartjs2.html">View Leaves</a></li>
                        </ul>
                    </li>-->
                    <!--<li>
                        <a><i class="fa fa-clone"></i>Notice <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="../noticeBoard/noticeBoard.php">Notice Board</a></li>
                        </ul>
                    </li>-->
                </ul>
            </div>
            <div class="menu_section">
               <!-- <h3>Live On</h3>
                <ul class="nav side-menu">
                    <li>
                        <a><i class="fa fa-laptop"></i> Live Event <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="../liveEvent/liveEventHistory.php">View Live Event</a></li>
                        </ul>
                    </li>
                </ul>-->
            </div>
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="../../index.php">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>

<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="../../assets/images/300.jpg" alt=""><strong>Employee</strong>
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="javascript:;"> Profile</a></li>
                        <li><a href="../logout/logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                    </ul>
                </li>

                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->