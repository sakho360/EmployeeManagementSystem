<?php

require_once "../../vendor/autoload.php";

use Sandwitch\Expenses\Expenses;
use Sandwitch\Logger\UserMessage;

$userMessage = new UserMessage();
$expenses = new Expenses($userMessage);
$expenses->add($_POST);