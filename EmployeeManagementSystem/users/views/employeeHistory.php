<?php

require_once "../../vendor/autoload.php";
session_start();

use Sandwitch\Department\Department;
use Sandwitch\Designation\Designation;
use Sandwitch\Logger\DesignationMessage;
use Sandwitch\Logger\DepartmentMessage;
use Sandwitch\Logger\UserMessage;
use Sandwitch\Employee\Employee;

$userMessage = new UserMessage();
$employee = new Employee($userMessage);
$employees = $employee->show($_SESSION['id']);

$departmentMessage = new DepartmentMessage();
$department = new Department($departmentMessage);

$designationMessage = new DesignationMessage();
$designation = new Designation($designationMessage);
?>

<?php
require_once "../elements/header.php";

?>

<body class="nav-md">
<div class="container body">
    <div class="main_container">

        <!-- side and top bar include -->
        <?php include '../elements/nav.php' ?>
        <!-- /side and top bar include -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="page-title">
                    <div class="title_left">
                        <h3>Show Your Information</h3>
                    </div>

                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Manage <small>Employee</small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">

                                <p class="text-muted font-13 m-b-30">

                                </p>

                                <table id="datatable-buttons" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Employee Name</th>
                                        <th>User Name</th>
                                        <th>Password</th>
                                        <th>Email</th>
                                        <th>Phone No.</th>
                                        <th>Department</th>
                                        <th>Designations</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($employees as $employee){

                                        $departments = $department->selectDepartmentByEmployee($employee->id);

                                        $designations = $designation->getDesignationByEmployee($employee->id);


                                        ?>
                                        <tr>
                                            <td><?=$employee->first_name." ".$employee->last_name; ?></td>
                                            <td><?=$employee->user_name; ?></td>
                                            <td><?=$employee->password; ?></td>
                                            <td><?=$employee->email; ?></td>
                                            <td><?=$employee->mobile_no; ?></td>
                                            <td><?= $departments->name; ?></td>
                                            <td><?= $designations->title; ?></td>
                                            <td><?php
                                                if ($employee->status == 1){
                                                    echo "Active";
                                                }else{
                                                    echo "Inactive";
                                                }

                                                ?></td>

                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content include -->
        <?php include '../elements/footer.php' ?>
        <!-- /footer content include -->
    </div>
</div>

<!--Script-->
<?php require_once "../elements/script.php";?>
<!--Script-->

</body>
</html>

